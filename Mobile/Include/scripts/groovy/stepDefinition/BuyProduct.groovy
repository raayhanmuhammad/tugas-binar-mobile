package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.And
import cucumber.api.java.en.Then
import internal.GlobalVariable

public class BuyProduct {
	//UNTUK LOGIN
	@Given("user already open the app and login")
	public void user_already_open_the_app_and_login() {
		Mobile.startApplication('./Apk/app-release.apk', true)
		Mobile.verifyElementVisible(findTestObject('Page_Home/btn_akun'), 0)
		Mobile.delay(3)
		Mobile.tap(findTestObject('Page_Home/btn_akun'), 0)
		Mobile.delay(3)
		Mobile.tap(findTestObject('Page_Akun/Before Login/btn_masuk'), 0)
		Mobile.delay(3)
		Mobile.setText(findTestObject('Page_Login/inputfield_email'), 'hi.julea@gmail.com', 0)
		Mobile.delay(3)
		Mobile.setText(findTestObject('Page_Login/inputfield_password'), 'Bintang7*', 0)
		Mobile.delay(3)
		Mobile.tap(findTestObject('Page_Login/btn_masuk'), 0)
	}

	@When("user is in homepage")
	public void user_is_in_homepage() {
		Mobile.verifyElementVisible(findTestObject('Page_Home/btn_akun'), 0)
	}

	@When("user tap the akun button")
	public void user_tap_the_akun_button() {
		Mobile.tap(findTestObject('Page_Home/btn_akun'), 0)
	}

	@When("user tap the masuk button")
	public void user_tap_the_masuk_button() {
		Mobile.tap(findTestObject('Page_Akun/Before Login/btn_masuk'), 0)
	}

	@When("user fill the email field with valid buyer email")
	public void user_fill_the_email_field_with_email() {
		Mobile.setText(findTestObject('Page_Login/inputfield_email'), 'hi.julea@gmail.com', 0)
	}

	@When("user fill the password field with valid buyer password")
	public void user_fill_the_password_field_with_password() {
		Mobile.setText(findTestObject('Page_Login/inputfield_password'), 'Bintang7*', 0)
	}

	@When("user tap the masuk login button")
	public void user_tap_the_masuk_login_button() {
		Mobile.tap(findTestObject('Page_Login/btn_masuk'), 0)
	}

	@Then("user can succesfully login")
	public void user_can_login_(String result) {

	}

	// UNTUK BUY PRODUCT
	@When("user search a product")
	public void user_search_a_product( ) {
		Mobile.delay(3)
		Mobile.tap(findTestObject('Page_Buy_Product/btn_beranda'),0)
		Mobile.delay(5)
		Mobile.tap(findTestObject('Page_Buy_Product/btn_searchbox'),0)
		Mobile.delay(3)
		Mobile.setText(findTestObject('Page_Buy_Product/textfield_searchbox'), 'Boneka',0)
	}

	@When("user tap a product")
	public void user_tap_a_product() {
		Mobile.delay(5)
		Mobile.tap(findTestObject('Page_Buy_Product/Image_Product'),0)
	}

	@When("user tap Saya tertarik dan ingin nego button")
	public void user_tap_Saya_tertarik_dan_ingin_nego_button() {
		Mobile.delay(5)
		Mobile.tap(findTestObject('Page_Buy_Product/btn_nego'),0)
	}

	@When("user input (.*) into price field")
	public void user_input_into_price_field(String Price) {
		if(Price=='valid') {
			Mobile.delay(5)
			Mobile.setText(findTestObject('Page_Buy_Product/input_price' ), '1000000',0)
		}
		else if(Price=='empty') {
			Mobile.delay(5)
			Mobile.setText(findTestObject('Page_Buy_Product/input_price'), '',0)
		}
		else if(Price=='zero') {
			Mobile.delay(5)
			Mobile.setText(findTestObject('Page_Buy_Product/input_price'),'0',0)
		}
		else if(Price=='string') {
			Mobile.delay(5)
			Mobile.setText(findTestObject('Page_Buy_Product/input_price'), 'abcde',0)
		}
		else if(Price=='character') {
			Mobile.delay(5)
			Mobile.setText(findTestObject('Page_Buy_Product/input_price'), '-@#$@', 0)
		}
	}

	@When("user tap Kirim button")
	public void user_tap_Kirim_button() {
		Mobile.tap(findTestObject('Page_Buy_Product/btn_bid'), 0)
	}

	@Then("user can see Failed notification")
	public void user_can_see_Failed_notification() {

	}

	@Then("user can see Success notification")
	public void user_can_see_Success_notification() {
		Mobile.verifyElementVisible(findTestObject('Page_Buy_Product/notif_berhasil'),0)
	}

}
